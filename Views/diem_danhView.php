<?php if(isset($data['students']) && $data['students'] != false): ?>
<form id="form_conf" action="<?php echo url_action('diem_danh') ?>" method="post">
    <input type="hidden" name="get_list" value="">
    <input type="hidden" name="get_list_2" value="">
    <input type="hidden" name="date" value="<?php echo $_POST['date'] ?>">
    <input type="hidden" name="buoi" value="<?php echo $_POST['buoi'] ?>">
    <input type="hidden" name="class_id" value="<?php echo $_POST['class_id'] ?>">
    <input type="hidden" name="data" id="inp_data">
</form>
<!--begin::Card-->
<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Scrollable Table
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            <a href="javascript:void(0)" onclick="check_form()" class="btn btn-primary font-weight-bolder">
            <span class="svg-icon svg-icon-md">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <circle fill="#000000" cx="9" cy="15" r="6" />
                        <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>Lưu Lại Thay Đổi</a>
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Datatable-->
        <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Họ và tên</th>
                    <th>Ngày sinh</th>
                    <th>Giới tính</th>
                    <th>Địa chỉ</th>
                    <th>Trạng thái</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($data['students'] as $student): ?>
                <tr onclick="changeActive(this)" class="row_students <?php echo $student['status'] == 1 ? 'active' : '' ?>">
                    <td><?php echo $student['id'] ?></td>
                    <td><?php echo $student['name'] ?></td>
                    <td><?php echo convertDate($student['birthday'], 'd/m/Y') ?></td>
                    <td><?php echo sex_string($student['sex']) ?></td>
                    <td><?php echo $student['address'] ?></td>
                    <td><?php echo $student['status'] == 1 ? 'Có mặt' : 'Vắng mặt' ?></td>
                </tr>
                <?php endforeach ?>
            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>
<!--end::Card-->
<?php endif ?>


<!--begin::Card-->
<div class="card card-custom gutter-b example example-compact">
    <div class="card-header">
        <h3 class="card-title">Chọn thời gian đánh dấu điểm danh</h3>
    </div>
    <!--begin::Form-->
    <form class="form" action="<?php echo url_action('diem_danh') ?>" method="post">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group">
                        <label class="col-form-label text-right">Chọn ngày điểm danh</label>
                        <div class="input-group date">
                            <input name="date" type="text" class="form-control" readonly="readonly" value="<?php echo isset($_POST['date']) ? convertDate($_POST['date'], 'm/d/Y') : '' ?>" id="kt_datepicker_3" />
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="col-form-label text-right">Chọn ngày buổi điểm danh</label>
                        <select class="form-control selectpicker" name="buoi">
                            <option value="0">Buổi sáng</option>
                            <option value="1">Buổi chiều</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="col-form-label text-right">Chọn lớp</label>
                        <select class="form-control selectpicker" name="class_id">
                            <?php foreach($data['classes'] as $class): ?>
                            <option value="<?php echo $class['id'] ?>"><?php echo $class['class_name'] ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <button type="submit" name="get_list" class="btn btn-primary mr-2">Xác nhận</button>
        </div>
    </form>
    <!--end::Form-->
</div>
<!--end::Card-->

<script>
    function changeActive(e) {
        if($(e).hasClass('active')) {
            $(e).removeClass('active');
            $(e).children().last().text('Vắng mặt');
        } else {
            $(e).addClass('active')
            $(e).children().last().text('Có mặt');
        }
    }

    function check_form() {
        var data = $('.row_students');
        var element = false;
        var resualt = [];
        for (let index = 0; index < data.length; index++) {
            element = data[index];
            resualt.push($(element).hasClass('active'));
        }
        
        $('#inp_data').val(JSON.stringify(resualt));
        $('#form_conf').submit();
    }
</script>