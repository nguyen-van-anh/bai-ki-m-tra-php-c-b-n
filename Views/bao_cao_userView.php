
<!--begin::Card-->
<div class="card card-custom gutter-b example example-compact">
    <div class="card-header">
        <h3 class="card-title">Chọn thời gian đánh dấu điểm danh cho sinh viên: <?php echo $data['student']['name'] ?></h3>
    </div>
    <!--begin::Form-->
    <form class="form" action="<?php echo url_action('diem_danh') ?>" method="post">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group">
                        <p class="text-left">Tổng số buổi tham gia điểm danh: <?php echo $data['thong_ke']['total'] ?> Buổi</p>
                        <p class="text-left">Có mặt: <?php echo $data['thong_ke']['co_mat'] ?> Buổi</p>
                        <p class="text-left">Vắng mặt: <?php echo $data['thong_ke']['vang_mat'] ?> Buổi</p>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label text-right">Chọn khoảng thời gian cần kiểm tra</label>
                    
                        <div class="input-daterange input-group" id="kt_datepicker_5">
                            <input type="text" class="form-control" name="start" />
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-ellipsis-h"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" name="end" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <button type="submit" name="get_list" class="btn btn-primary mr-2">Xác nhận</button>
        </div>
    </form>
    <!--end::Form-->
</div>
<!--end::Card-->
