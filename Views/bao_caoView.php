
<!--begin::Card-->
<div class="card card-custom gutter-b example example-compact">
    <div class="card-header">
        <h3 class="card-title">Báo cáo sĩ số điểm danh lớp: <?php echo $data['class']['class_name'] ?></h3>
    </div>

    <!--begin::Form-->
    <form class="form" action="/" method="get">
        <input type="hidden" name="action" value="bao_cao">
        <div class="card-body">
            <div class="form-group  row">
                <div class="col-sm-6">
                    <label class="col-form-label text-right">Chọn lớp cần xem báo cáo</label>
                    <select class="form-control selectpicker" name="class">
                        <?php foreach($data['classes'] as $class): ?>
                        <option value="<?php echo $class['id'] ?>"><?php echo $class['class_name'] ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <!--end::Form-->
        </div>

        <div class="card-footer text-right">
            <button type="submit" class="btn btn-primary mr-2">Xác nhận</button>
        </div>
    </form>
</div>
