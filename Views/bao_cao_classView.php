
<!--begin::Card-->
<div class="card card-custom gutter-b example example-compact">
    <div class="card-header">
        <h3 class="card-title">Báo cáo sĩ số điểm danh lớp: <?php echo $data['class']['class_name'] ?></h3>
    </div>

    <!--begin::Form-->
    <form class="form" action="<?php echo url_action('bao_cao', ['class' => $data['class']['id']]) ?>" method="post">
        <div class="card-body">
            <p><strong>Name: </strong><?php echo $data['class']['class_name'] ?> | <strong>Sĩ số: </strong><?php echo $data['class']['count'] ?> | <strong>Quản lý: </strong> <?php echo $GLOBALS['authencation']->user['username'] ?></p>

            <div class="form-group  row">
                <div class="col-sm-6">
                    <label class="col-form-label text-right">Chọn khoảng thời gian cần kiểm tra</label>
                
                    <div class="input-daterange input-group" id="kt_datepicker_5">
                        <input type="text" class="form-control" name="start" />
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="la la-ellipsis-h"></i>
                            </span>
                        </div>
                        <input type="text" class="form-control" name="end" />
                    </div>
                </div>
            </div>
            <!--end::Form-->
        </div>

        <div class="card-footer text-right">
            <button type="submit" name="get_list" class="btn btn-primary mr-2">Xác nhận</button>
        </div>
    </form>
</div>
<!--end::Card-->

<!--begin::Card-->
<div class="card card-custom gutter-b">
    <div class="card-header">
        <div class="card-title">
            <h3 class="card-label">Biểu đồ Báo cáo điểm danh 
                <?php if(isset($_POST['get_list'])): ?> 
                    từ <?php echo convertDate($_POST['start'], 'd/m/Y') ?> đến <?php echo convertDate($_POST['start'], 'd/m/Y') ?>  <?php else: ?>tuần này<?php endif ?></h3>
        </div>
    </div>
    <div class="card-body">
        <div id="kt_amcharts_3" style="height: 500px;"></div>
    </div>
</div>
<!--end::Card-->

<script>
    var configDemo1 = [
        <?php foreach($data['date_data'] as $index => $data): ?>
        {
            "comat": "<?php echo $index ?>",
            "sang": <?php echo $data['sang']['co'] ?>,
            "chieu": <?php echo $data['chieu']['co'] ?>
        },
        <?php endforeach ?>
        ];
</script>