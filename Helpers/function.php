<?php


function get_info_menu()
{
    return [
        [
            'name' => 'Danh sách sinh viên',
            'link' => url_action('home'),
            'action' => 'home'
        ],
        [
            'name' => 'Điểm danh sinh viên',
            'link' => url_action('diem_danh'),
            'action' => 'diem_danh'
        ],
        [
            'name' => 'Báo cáo sinh viên',
            'link' => url_action('bao_cao'),
            'action' => 'bao_cao'
        ]
    ];
}

function url_action($action, $get_data = [])
{
    $add_data = '';
    foreach ($get_data as $key => $value) {
        $add_data .= "&{$key}={$value}";
    }

    return 'http://' . $_SERVER['HTTP_HOST'] . '/index.php?action=' . $action . $add_data;
}
function redirect_action($action,$get_data = [])
{
    header('Location: ' . url_action($action, $get_data));
    die();
}
function getActionRequest()
{
    if(isset($_REQUEST['action']) && file_exists(getPathAction($_REQUEST['action']))) {
        return getPathAction($_REQUEST['action']);
    } else {
        return getPathAction('errors');
    }
}

function getPathView($name)
{
    return __DIR__ . '/../Views/' . $name . 'View.php';
}

function getPathLayout($name)
{
    return __DIR__ . '/../Layouts/' . $name . 'View.php';
}

function getPathAction($action)
{
    return __DIR__ . '/../Actions/' . $action . 'Action.php';
}

function return_layout_view($name, $data, $layout)
{
    if(file_exists(getPathLayout($layout)))
    require_once(getPathLayout($layout));
}

function return_view($name, $data = [], $layout = false)
{
    if($layout != false ) {
        return_layout_view($name, $data, $layout);
    } else {
        if(file_exists(getPathView($name))) {
            require_once(getPathView($name));
        } else  {
            die('View Not Found!');
        }
    }
}

function date_format_string($date)
{
    return $date;
}

function sex_string($num)
{
    return $num == 2 ? 'Nam' : 'Nữ';
}

function middleware_login($status)
{
    # Check login : 
    if($status == $GLOBALS['authencation']->status) {
        return true;
    }
    return redirect_action('errors');
}

function dump_data($data)
{
    print_r('<pre>');
    print_r($data);
    print_r('</pre>');
    die();
}

function count_index($arr, $index, $value)
{
    $dem = 0;
    foreach($arr as $a) {
        if($a[$index] == $value) $dem ++;
    }
    return $dem;
}

function convertDate($source,$from)
{
    $date = new DateTime($source);
    return $date->format($from);
}

?>