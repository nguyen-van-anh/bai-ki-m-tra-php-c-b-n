<?php 
middleware_login(true);

$classes = $connection->all("SELECT * FROM classes");

$data_students = false;

if(isset($_POST['get_list'])) {
    $_POST['date'] = convertDate($_POST['date'], 'Y-m-d');
    
    $data_students = $connection->all("SELECT *, st_j.id as root_id FROM student_join st_j INNER JOIN students st ON st_j.student_id = st.id WHERE date_join = '{$_POST['date']}' AND st.class_id = {$_POST['class_id']} AND buoi = {$_POST['buoi']}");

    if(count($data_students) == 0) {
        $students = $connection->all("SELECT * FROM students WHERE class_id = {$_POST['class_id']}");
        
        foreach($students as $student) {
            $connection->once("INSERT INTO `diem_danh`.`student_join` (`student_id`, `user_id`, `date_join`, `buoi`, `status`) VALUES ({$student['id']}, {$authencation->user['id']}, '{$_POST['date']}', {$_POST['buoi']}, 3)");
        }

        $data_students = $connection->all("SELECT *, st_j.id as root_id FROM student_join st_j INNER JOIN students st ON st_j.student_id = st.id WHERE date_join = '{$_POST['date']}' AND st.class_id = {$_POST['class_id']} AND buoi = {$_POST['buoi']}");
    }

}

if(isset($_POST['get_list_2'])) {
    $data = json_decode($_POST['data'], true);

    foreach ($data_students as $index => $student) {
        $status = $data[$index] ? 1 : 2;
        $connection->once("UPDATE `diem_danh`.`student_join` SET `status` = {$status} WHERE (`id` = {$student['root_id']})");
    }
    
    $data_students = $connection->all("SELECT *, st_j.id as root_id FROM student_join st_j INNER JOIN students st ON st_j.student_id = st.id WHERE date_join = '{$_POST['date']}' AND st.class_id = {$_POST['class_id']} AND buoi = {$_POST['buoi']}");

}


return_view('diem_danh', [
    'students' => $data_students,
    'classes' => $classes,
], 'admin');

?>