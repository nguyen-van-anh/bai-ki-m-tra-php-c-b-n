<?php
middleware_login(true);

$student = false;
if(isset($_GET['user'])) {
    $student = $connection->once("SELECT * FROM students WHERE id = {$_GET['user']}");
    $student_join = $connection->all("SELECT * FROM student_join WHERE student_id = {$_GET['user']}");

    $thong_ke = [
        'total' => count($student_join),
        'co_mat' => count_index($student_join, 'status', 1),
        'vang_mat' => count($student_join) - count_index($student_join, 'status', 1)
    ];
    return_view('bao_cao_user', [
        'student' => $student,
        'thong_ke' => $thong_ke
    ], 'admin');
} elseif(isset($_GET['class'])) {
    $class = $connection->once("SELECT * FROM classes WHERE id = {$_GET['class']}");
    $student_join_sang = $connection->all("SELECT count(*) as co , date_join FROM student_join WHERE student_id in (SELECT id FROM students WHERE class_id = {$_GET['class']}) AND status = 1 AND buoi = 0 GROUP BY date_join");
    $student_join_chieu = $connection->all("SELECT count(*) as co , date_join FROM student_join WHERE student_id in (SELECT id FROM students WHERE class_id = {$_GET['class']}) AND status = 1 AND buoi = 1 GROUP BY date_join");

    $student_join_date_s = [];
    $student_join_date_c = [];

    foreach($student_join_sang as $stu){
        $student_join_date_s[$stu['date_join']] = [
            'date_join' => $stu['date_join'],
            'co' => $stu['co'],
            'vang' => $class['count'] - $stu['co'],
        ];
    }
    foreach($student_join_chieu as $stu){
        $student_join_date_c[$stu['date_join']] = [
            'date_join' => $stu['date_join'],
            'co' => $stu['co'],
            'vang' => $class['count'] - $stu['co'],
        ];
    }
    if(isset($_POST['get_list'])) {
        $start = new DateTime($_POST['start']);
        $end = new DateTime($_POST['end']);
    } else {
        $start = new DateTime('monday this week');
        $end = new DateTime('monday next week');
    }
    
    $interval = DateInterval::createFromDateString('1 day');
    $period = new DatePeriod($start, $interval, $end);

    $date_data = [];

    foreach ($period as $dt) {
        $status_s = isset($student_join_date_s[$dt->format('Y-m-d')]);
        $status_c = isset($student_join_date_c[$dt->format('Y-m-d')]);
        $date_data[$dt->format('Y-m-d')] = [
            'sang' => [
                'total' => $class['count'],
                'status' => $status_s,
                'co' => $status_s ? $student_join_date_s[$dt->format('Y-m-d')]['co'] : 0,
                'vang' => $status_s ? $student_join_date_s[$dt->format('Y-m-d')]['vang'] : 0,
            ],
            'chieu' => [
                'total' => $class['count'],
                'status' => $status_c,
                'co' => $status_c ? $student_join_date_c[$dt->format('Y-m-d')]['co'] : 0,
                'vang' => $status_c ? $student_join_date_c[$dt->format('Y-m-d')]['vang'] : 0,
            ],
        ];
    }
    
    return_view('bao_cao_class', [
        'class' => $class,
        'date_data' => $date_data
    ], 'admin');
} else {

    $classes = $connection->all("SELECT * FROM classes");

    return_view('bao_cao', [
        'classes' => $classes
    ], 'admin');
}



?>