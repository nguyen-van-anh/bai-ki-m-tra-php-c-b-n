<?php
middleware_login(true);

$students = $connection->all("SELECT *, st.id as root_id FROM students st INNER JOIN classes cl on st.class_id = cl.id ");

return_view('home', [
    'students' => $students
], 'admin');

?>