-- MySQL dump 10.13  Distrib 8.0.22, for Linux (x86_64)
--
-- Host: localhost    Database: diem_danh
-- ------------------------------------------------------
-- Server version	8.0.22-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `classes`
--

DROP TABLE IF EXISTS `classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `classes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `class_name` varchar(45) NOT NULL,
  `count` int DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classes`
--

LOCK TABLES `classes` WRITE;
/*!40000 ALTER TABLE `classes` DISABLE KEYS */;
INSERT INTO `classes` VALUES (1,'Lập trình và thiết kế Website',11),(2,'Ứng dụng phần mềm',8);
/*!40000 ALTER TABLE `classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_join`
--

DROP TABLE IF EXISTS `student_join`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_join` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_id` int NOT NULL,
  `user_id` int NOT NULL,
  `date_join` date NOT NULL,
  `buoi` tinyint DEFAULT '0',
  `status` int NOT NULL,
  `note` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=230 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_join`
--

LOCK TABLES `student_join` WRITE;
/*!40000 ALTER TABLE `student_join` DISABLE KEYS */;
INSERT INTO `student_join` VALUES (68,1,1,'2020-11-25',0,2,NULL),(69,2,1,'2020-11-25',0,1,NULL),(70,3,1,'2020-11-25',0,2,NULL),(71,4,1,'2020-11-25',0,2,NULL),(72,5,1,'2020-11-25',0,1,NULL),(73,6,1,'2020-11-25',0,2,NULL),(74,7,1,'2020-11-25',0,1,NULL),(75,8,1,'2020-11-25',0,2,NULL),(76,9,1,'2020-11-25',0,1,NULL),(77,10,1,'2020-11-25',0,2,NULL),(78,11,1,'2020-11-25',0,1,NULL),(79,1,1,'2020-11-17',1,2,NULL),(80,2,1,'2020-11-17',1,1,NULL),(81,3,1,'2020-11-17',1,2,NULL),(82,4,1,'2020-11-17',1,2,NULL),(83,5,1,'2020-11-17',1,1,NULL),(84,6,1,'2020-11-17',1,2,NULL),(85,7,1,'2020-11-17',1,2,NULL),(86,8,1,'2020-11-17',1,1,NULL),(87,9,1,'2020-11-17',1,2,NULL),(88,10,1,'2020-11-17',1,2,NULL),(89,11,1,'2020-11-17',1,2,NULL),(90,1,1,'2020-11-27',0,1,NULL),(91,2,1,'2020-11-27',0,1,NULL),(92,3,1,'2020-11-27',0,1,NULL),(93,4,1,'2020-11-27',0,1,NULL),(94,5,1,'2020-11-27',0,1,NULL),(95,6,1,'2020-11-27',0,1,NULL),(96,7,1,'2020-11-27',0,1,NULL),(97,8,1,'2020-11-27',0,1,NULL),(98,9,1,'2020-11-27',0,1,NULL),(99,10,1,'2020-11-27',0,1,NULL),(100,11,1,'2020-11-27',0,1,NULL),(101,1,1,'2020-11-28',0,3,NULL),(102,2,1,'2020-11-28',0,3,NULL),(103,3,1,'2020-11-28',0,3,NULL),(104,4,1,'2020-11-28',0,3,NULL),(105,5,1,'2020-11-28',0,3,NULL),(106,6,1,'2020-11-28',0,3,NULL),(107,7,1,'2020-11-28',0,3,NULL),(108,8,1,'2020-11-28',0,3,NULL),(109,9,1,'2020-11-28',0,3,NULL),(110,10,1,'2020-11-28',0,3,NULL),(111,11,1,'2020-11-28',0,3,NULL),(112,12,1,'2020-11-17',0,1,NULL),(113,13,1,'2020-11-17',0,1,NULL),(114,14,1,'2020-11-17',0,1,NULL),(115,15,1,'2020-11-17',0,1,NULL),(116,16,1,'2020-11-17',0,1,NULL),(117,17,1,'2020-11-17',0,1,NULL),(118,18,1,'2020-11-17',0,1,NULL),(119,19,1,'2020-11-17',0,1,NULL),(120,1,1,'2020-12-04',0,1,NULL),(121,2,1,'2020-12-04',0,1,NULL),(122,3,1,'2020-12-04',0,1,NULL),(123,4,1,'2020-12-04',0,1,NULL),(124,5,1,'2020-12-04',0,1,NULL),(125,6,1,'2020-12-04',0,1,NULL),(126,7,1,'2020-12-04',0,1,NULL),(127,8,1,'2020-12-04',0,1,NULL),(128,9,1,'2020-12-04',0,1,NULL),(129,10,1,'2020-12-04',0,1,NULL),(130,11,1,'2020-12-04',0,1,NULL),(131,1,1,'2020-11-16',0,1,NULL),(132,2,1,'2020-11-16',0,1,NULL),(133,3,1,'2020-11-16',0,1,NULL),(134,4,1,'2020-11-16',0,1,NULL),(135,5,1,'2020-11-16',0,1,NULL),(136,6,1,'2020-11-16',0,1,NULL),(137,7,1,'2020-11-16',0,1,NULL),(138,8,1,'2020-11-16',0,1,NULL),(139,9,1,'2020-11-16',0,1,NULL),(140,10,1,'2020-11-16',0,1,NULL),(141,11,1,'2020-11-16',0,1,NULL),(142,1,1,'2020-11-16',1,1,NULL),(143,2,1,'2020-11-16',1,2,NULL),(144,3,1,'2020-11-16',1,1,NULL),(145,4,1,'2020-11-16',1,1,NULL),(146,5,1,'2020-11-16',1,1,NULL),(147,6,1,'2020-11-16',1,1,NULL),(148,7,1,'2020-11-16',1,2,NULL),(149,8,1,'2020-11-16',1,1,NULL),(150,9,1,'2020-11-16',1,1,NULL),(151,10,1,'2020-11-16',1,1,NULL),(152,11,1,'2020-11-16',1,2,NULL),(153,1,1,'2020-11-17',0,1,NULL),(154,2,1,'2020-11-17',0,1,NULL),(155,3,1,'2020-11-17',0,1,NULL),(156,4,1,'2020-11-17',0,2,NULL),(157,5,1,'2020-11-17',0,1,NULL),(158,6,1,'2020-11-17',0,1,NULL),(159,7,1,'2020-11-17',0,2,NULL),(160,8,1,'2020-11-17',0,1,NULL),(161,9,1,'2020-11-17',0,1,NULL),(162,10,1,'2020-11-17',0,2,NULL),(163,11,1,'2020-11-17',0,1,NULL),(164,1,1,'2020-11-18',0,1,NULL),(165,2,1,'2020-11-18',0,1,NULL),(166,3,1,'2020-11-18',0,1,NULL),(167,4,1,'2020-11-18',0,1,NULL),(168,5,1,'2020-11-18',0,1,NULL),(169,6,1,'2020-11-18',0,1,NULL),(170,7,1,'2020-11-18',0,1,NULL),(171,8,1,'2020-11-18',0,1,NULL),(172,9,1,'2020-11-18',0,1,NULL),(173,10,1,'2020-11-18',0,1,NULL),(174,11,1,'2020-11-18',0,1,NULL),(175,1,1,'2020-11-18',1,1,NULL),(176,2,1,'2020-11-18',1,1,NULL),(177,3,1,'2020-11-18',1,2,NULL),(178,4,1,'2020-11-18',1,1,NULL),(179,5,1,'2020-11-18',1,1,NULL),(180,6,1,'2020-11-18',1,2,NULL),(181,7,1,'2020-11-18',1,1,NULL),(182,8,1,'2020-11-18',1,2,NULL),(183,9,1,'2020-11-18',1,1,NULL),(184,10,1,'2020-11-18',1,1,NULL),(185,11,1,'2020-11-18',1,2,NULL),(186,1,1,'2020-11-19',0,1,NULL),(187,2,1,'2020-11-19',0,1,NULL),(188,3,1,'2020-11-19',0,1,NULL),(189,4,1,'2020-11-19',0,2,NULL),(190,5,1,'2020-11-19',0,2,NULL),(191,6,1,'2020-11-19',0,2,NULL),(192,7,1,'2020-11-19',0,1,NULL),(193,8,1,'2020-11-19',0,1,NULL),(194,9,1,'2020-11-19',0,2,NULL),(195,10,1,'2020-11-19',0,2,NULL),(196,11,1,'2020-11-19',0,2,NULL),(197,1,1,'2020-11-19',1,1,NULL),(198,2,1,'2020-11-19',1,1,NULL),(199,3,1,'2020-11-19',1,1,NULL),(200,4,1,'2020-11-19',1,1,NULL),(201,5,1,'2020-11-19',1,1,NULL),(202,6,1,'2020-11-19',1,1,NULL),(203,7,1,'2020-11-19',1,1,NULL),(204,8,1,'2020-11-19',1,1,NULL),(205,9,1,'2020-11-19',1,1,NULL),(206,10,1,'2020-11-19',1,1,NULL),(207,11,1,'2020-11-19',1,1,NULL),(208,1,1,'2020-11-20',0,1,NULL),(209,2,1,'2020-11-20',0,1,NULL),(210,3,1,'2020-11-20',0,1,NULL),(211,4,1,'2020-11-20',0,1,NULL),(212,5,1,'2020-11-20',0,1,NULL),(213,6,1,'2020-11-20',0,1,NULL),(214,7,1,'2020-11-20',0,1,NULL),(215,8,1,'2020-11-20',0,1,NULL),(216,9,1,'2020-11-20',0,1,NULL),(217,10,1,'2020-11-20',0,1,NULL),(218,11,1,'2020-11-20',0,2,NULL),(219,1,1,'2020-11-20',1,1,NULL),(220,2,1,'2020-11-20',1,1,NULL),(221,3,1,'2020-11-20',1,1,NULL),(222,4,1,'2020-11-20',1,1,NULL),(223,5,1,'2020-11-20',1,1,NULL),(224,6,1,'2020-11-20',1,1,NULL),(225,7,1,'2020-11-20',1,1,NULL),(226,8,1,'2020-11-20',1,1,NULL),(227,9,1,'2020-11-20',1,1,NULL),(228,10,1,'2020-11-20',1,1,NULL),(229,11,1,'2020-11-20',1,1,NULL);
/*!40000 ALTER TABLE `student_join` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `students` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `sex` tinyint DEFAULT '0',
  `birthday` date DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `class_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_students_classes_idx` (`class_id`),
  CONSTRAINT `fk_students_classes` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES (1,'Nguyễn Văn Anh',2,'2001-05-10','Nghệ An',1),(2,'Nguyễn Văn Tuấn',2,'1999-05-05','Hà Nội',1),(3,'Nguyễn Văn Tủyên',2,'1998-03-23','Tuyên Quang',1),(4,'Bùi Đức Sơn',2,'2001-09-09','Hải Dương',1),(5,'Phạm Văn Hoàng',2,'2001-03-03','Thanh Hóa',1),(6,'Nguyễn Văn Thái',2,'2001-05-09','Nghệ An',1),(7,'Nguyễn Hải Anh',2,'2000-02-02','Hà Nội',1),(8,'Nguyễn Văn Thủy',2,'2001-02-02','Hà Nội',1),(9,'Nguyễn Văn Hiếu',2,'2001-03-03','Hà Nội',1),(10,'Bùi Văn Thắng',2,'2000-09-09','Hà Nội',1),(11,'Nguyễn Anh Minh',2,'2000-02-02','Hà Nội',1),(12,'Bùi Anh Tú',2,'2001-08-08','Hà Nội',2),(13,'Phạm Quỳnh Trang',1,'2001-09-09','Hà Nội',2),(14,'Hà Văn Tú',2,'2001-09-09','Quảng Ngãi',2),(15,'Trịnh Văn Bằng',2,'2000-07-07','Ninh Bình',2),(16,'Hoàng Văn Nghĩa',2,'1999-08-08','Thanh Hóa',2),(17,'Hoàng Thị Thắm',1,'1999-03-04','Huế',2),(18,'Đinh Tiến Mạnh',2,'1999-09-09','Quảng Bình',2),(19,'Hoàng Trung Kiên',2,'2000-10-10','Nghệ An',2);
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','e75f9351aedf42ead332f8225aab9018',NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-17 13:39:30
