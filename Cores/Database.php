<?php 

class Database
{
    private const HOST = '127.0.0.1';
    private const USERNAME = 'root';
    private const PASSWORD = 'vananh@2020';
    private const DBNAME = 'diem_danh';

    private $action = false;

    function __construct()
    {
        $this->action = new mysqli(static::HOST, static::USERNAME, static::PASSWORD, static::DBNAME);
        if ($this->action->connect_error) {
            die("Không kết nối :" . $this->action->connect_error);
            exit();
        }
    }

    function __destruct()
    {
        # close connect database 
        $this->action->close();
    }

    private function query($sql)
    {
        return $this->action->query($sql);
    }

    public function actionQuery($sql)
    {
        $resualt = [];

        $data = $this->query($sql);

        try {
            while ($row = $data->fetch_array(MYSQLI_ASSOC)) {
                $resualt[] = $row;
            }
        } catch (\Throwable $th) {
            
        }

        return $resualt;
    }

    public function once($sql)
    {
        $data = $this->actionQuery($sql);

        if(isset($data[0])) {
            return $data[0];
        } else {
            return false;
        }
    }

    public function all($sql)
    {
        return $this->actionQuery($sql);
    }

}

?>