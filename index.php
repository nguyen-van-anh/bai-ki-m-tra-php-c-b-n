<?php
    # stat section 
    session_start();
    date_default_timezone_set('Asia/Ho_Chi_Minh');
    
    # include function helpers 
    require_once(__DIR__ . '/Helpers/function.php');
    # include bootstrap with cores system 
    require_once(__DIR__ . '/Cores/bootstrap.php');
    
    $action_request_path = getActionRequest();
    
    require_once($action_request_path);
?>